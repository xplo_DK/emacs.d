;; .emacs

(package-initialize)

(setq gc-cons-threshold (* 50 1000 1000))

(require 'org)
(require 'rx)
(require 'bytecomp)

;; Searches an org document for '#+include:' tags.
;; It then compiles each of them and loads them.
;; (defun dk/compile-org (file)
;;   "Tangle any files which are included with the org document"
;;   (interactive)
;;   (message "Loading %s..." file)
;;   (with-current-buffer (find-file file)
;; 	(goto-char (point-min))
;; 	(while (re-search-forward (rx
;; 							   bol
;; 							   (eval "#+include:")
;; 							   space
;; 							   "\""
;; 							   (group (0+ (not (any space))))
;; 							   "\""
;; 							   (? space)
;; 							   (group (0+ (not (any space))))
;; 							   (? space)
;; 							   (group (0+ (not (any space))))
;; 							   (0+ space)
;; 							   eol)
;; 							  nil t)
;; 	  (let* ((lang (match-string 3))
;; 			(src (match-string 2))
;; 			(included-file (match-string 1)))
;; 		))))

(defun dk/compile-org (file)
  "Compile an org file to byte code"
  (interactive "fFile to compile: \n")
  (let* ((base-name (file-name-sans-extension file))
		(exported-file (concat base-name ".el")))
  (message "%s %s"
		   (progn (setq exported-file
				(car (last (org-babel-tangle-file file exported-file "emacs-lisp"))))
				  (byte-compile-file exported-file (concat exported-file "c"))
				  "Compiled")
		   exported-file)))

(defun dk/load-compiled-org (file)
  "Load a compiled org file"
  (interactive "fFile to load: \n")
  (let* ((age (lambda (file)
		(float-time
		 (time-since
		  (file-attribute-modification-time
		   (or (file-attributes (file-truename file))
		       (file-attributes file)))))))
	 (base-name (file-name-sans-extension file))
	 (exported-file (concat base-name ".elc")))
    (if (and (file-exists-p exported-file)
		 (<= (funcall age file) (funcall age exported-file)))
	  (load exported-file))
	(dk/compile-org file)))

(defun dk/on-save-compile-dot-emacs ()
  "Compile the current org file"
  (interactive)
  (with-no-warnings
	(let* ((major-mode (with-current-buffer (buffer-name) major-mode))
		   (dir (file-name-directory (buffer-file-name))))
	  (when (and (string= dir (expand-file-name user-emacs-directory))
				 (equal major-mode 'org-mode))
		(message "Compiling %s" (buffer-file-name))
		(dk/compile-org (buffer-file-name))))))

(add-hook 'after-save-hook 'dk/on-save-compile-dot-emacs)
(dk/load-compiled-org (concat user-emacs-directory "index.org"))
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))
