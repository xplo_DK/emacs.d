(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#1D252C" "#D95468" "#8BD49C" "#EBBF83" "#5EC4FF" "#E27E8D" "#70E1E8" "#A0B3C5"])
 '(c-basic-offset 4)
 '(c-default-style
   (quote
	((c-mode . "bsd")
	 (java-mode . "java")
	 (awk-mode . "awk")
	 (other . "gnu"))))
 '(custom-safe-themes
   (quote
	("df58ada3bf2590759ecbdb2334e942e1a2226138382923b95218d7856ab2195d" default)))
 '(doom-modeline-mode t)
 '(fci-rule-color "#56697A")
 '(global-emojify-mode t)
 '(hl-todo-keyword-faces
   (quote
	(("TODO" . "#3066A7")
	 ("NEXT" . "#3066A7")
	 ("THEM" . "#4573A6")
	 ("PROG" . "#5F6D8C")
	 ("OKAY" . "#5F6D8C")
	 ("DONT" . "#2D629C")
	 ("FAIL" . "#2D629C")
	 ("DONE" . "#596cbd")
	 ("NOTE" . "#3066A7")
	 ("KLUDGE" . "#3066A7")
	 ("HACK" . "#3066A7")
	 ("TEMP" . "#3066A7")
	 ("FIXME" . "#3066A7")
	 ("XXX+" . "#3066A7")
	 ("\\?\\?\\?+" . "#3066A7"))))
 '(hledger-currency-string "EUR")
 '(hledger-date-face (quote font-lock-type-face))
 '(hledger-description-face font-lock-keyword-face)
 '(hledger-year-of-birth 2001)
 '(jdee-db-active-breakpoint-face-colors (cons "#10151C" "#5EC4FF"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#10151C" "#8BD49C"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#10151C" "#384551"))
 '(objed-cursor-color "#D95468")
 '(org-agenda-files nil)
 '(org-modules
   (quote
	(org-bbdb org-bibtex org-docview org-eww org-gnus org-info org-irc org-mhe org-rmail org-tempo org-w3m org-notify)))
 '(org-structure-template-alist
   (quote
	(("c" . "center")
	 ("C" . "comment")
	 ("e" . "example")
	 ("E" . "export")
	 ("h" . "export html")
	 ("l" . "export latex")
	 ("q" . "quote")
	 ("s" . "src")
	 ("v" . "verse"))))
 '(org-tempo-keywords-alist
   (quote
	(("L" . "latex")
	 ("H" . "html")
	 ("A" . "ascii")
	 ("i" . "index")
	 ("t" . "title")
	 ("a" . "author"))))
 '(package-selected-packages
   (quote
	(pdf-tools treemacs-projectile treemacs-evil treemacs helm-lsp lsp-java company-lsp lsp-ui lsp-mode ess flycheck-rust flycheck hydra use-package-hydra git-timemachine helm-projectile hledger-mode multiple-cursors helm emojify company-emacs-eclim eclim darkroom writeroom-mode htmlize csv-mode evil-better-visual-line ranger rust-mode ewal-evil-cursors yasnippet company rainbow-delimiters doom-modeline ewal-spacemacs-themes spacemacs-theme ewal-spacemacs-theme ewal org-bullets smartparens use-package magit evil)))
 '(pdf-view-midnight-colors (quote ("#b1bccb" . "#121217")))
 '(send-mail-function (quote mailclient-send-it))
 '(tab-width 4)
 '(vc-annotate-background "#1D252C")
 '(vc-annotate-color-map
   (list
	(cons 20 "#8BD49C")
	(cons 40 "#abcd93")
	(cons 60 "#cbc68b")
	(cons 80 "#EBBF83")
	(cons 100 "#e5ae6f")
	(cons 120 "#df9e5b")
	(cons 140 "#D98E48")
	(cons 160 "#dc885f")
	(cons 180 "#df8376")
	(cons 200 "#E27E8D")
	(cons 220 "#df7080")
	(cons 240 "#dc6274")
	(cons 260 "#D95468")
	(cons 280 "#b05062")
	(cons 300 "#884c5c")
	(cons 320 "#604856")
	(cons 340 "#56697A")
	(cons 360 "#56697A")))
 '(vc-annotate-very-old-color nil)
 '(writeroom-bottom-divider-width 0)
 '(writeroom-fullscreen-effect (quote maximized))
 '(writeroom-restore-window-config t)
 '(writeroom-width 100))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-document-info ((t (:foreground "pale turquoise" :family "Source Sans Pro"))))
 '(org-document-title ((t (:foreground "#5EC4FF" :weight bold :height 2.0))))
 '(org-level-1 ((t (:inherit outline-1 :height 1.7))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
 '(org-link ((t (:foreground "#7b838e" :underline t))))
 '(which-key-group-description-face ((t (:inherit font-lock-type-face)))))
